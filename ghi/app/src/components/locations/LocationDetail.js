import { useParams, Link } from 'react-router-dom'
import { useState, useEffect} from 'react'

const LocationDetail = () => {
    const [location, setLocation] = useState({})
    const { id } = useParams()

    const getData = async () => {
        const resp = await fetch(`http://localhost:8000/api/locations/${id}`)
        if (resp.ok) {
            const data = await resp.json()
            setLocation(data)
        }
    }
    
    useEffect(()=> {
        getData()
    }, [])

    const handleDelete = async () => {
        const url = `http://localhost:8000/api/locations/${id}`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const resp = await fetch(url, fetchConfigs)
        const data = await resp.json()
    }

    return <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">   
                    <h1>Details Location</h1>
                    <h4>Name: <span>{location.name}</span></h4>
                    <h4>Location:  <span>{location.city}, {location.state}</span></h4>
                    <h4>Room Count: <span>{location.room_count}</span></h4>

                    <Link to='/locations' className="btn btn-primary">Return to Location List</Link>
                    <button onClick={handleDelete} className="btn btn-danger">Delete Location</button>
                </div>
            </div>
        </div>
    </>
}

export default LocationDetail;